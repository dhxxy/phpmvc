<div class="container mt-5">
    <div class="row">
        <div class="col-6">
            <h3>Blog</h3>
            <ul class="list-group">
            <?php foreach($data['blog'] as $blog): ?>
            <li class="list-group-item list-group-item d-flex justify-content-between align-items-center">
                <?= $blog['judul']; ?>
                <a href="<?= BASE_URL; ?>/blog/detail/<?= $blog['id']; ?>" class="btn btn-primary">Read</a>
                <a href="<?= BASE_URL; ?>/blog/detail/<?= $blog['id']; ?>" class="btn btn-danger">Delete</a>
                <a href="<?= BASE_URL; ?>/blog/detail/<?= $blog['id']; ?>" class="btn btn-dark">Edit</a>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
